YAGSbook
========

Documentation format for the YAGS roleplaying game. Includes stylesheets and
utilities. Based on Docbook, but simplified and with structures specific to
roleplaying games.

