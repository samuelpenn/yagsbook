<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
    xmlns:yb="http://yagsbook.sourceforge.net/xml"
    xmlns:y="http://yagsbook.sourceforge.net/xml/yags"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">

    
    <xsl:template match="y:list-spells">
        <fo:block
            font-weight="bold"
            color="{$colour}"
            font-size="{$font-large}"
            font-family="{$font-heading}"
            line-height="{$font-large}"
            space-after="{$font-medium}"
            text-align="start"
            keep-with-next="always">

            Spell List
        </fo:block>

        <fo:block space-after.optimum="12pt">
            <xsl:choose>
                <xsl:when test="@type">
                    <xsl:variable name="type" select="@type"/>
                    <xsl:apply-templates select="document(@href)/y:grimoire/y:spell[@type=$type]" mode="list">
                        <xsl:sort select="@level" data-type="number"/>
                        <xsl:sort select="@name" data-type="text"/>
                    </xsl:apply-templates>
                </xsl:when>

                <xsl:otherwise>
                    <xsl:apply-templates select="document(@href)/y:grimoire/y:spell" mode="list">
                        <xsl:sort select="@level" data-type="number"/>
                        <xsl:sort select="@name" data-type="text"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="y:spell" mode="list">
        <fo:block
                font-size="{$font-medium}"
                font-family="Times"
                line-height="12pt"
                space-after.optimum="{$font-medium}"
                text-align="start">
            <xsl:value-of select="@name"/> (<xsl:value-of select="@level"/>)            
        </fo:block>
        
        <xsl:variable name="name" select="@name"/>
        <fo:block
                font-size="{$font-medium}"
                font-family="Times"
                line-height="12pt"
                space-after.optimum="{$font-medium}"
                margin-left="3mm"
                text-align="start">
            <xsl:apply-templates select="../y:spell[y:prerequisite=$name]" mode="list"/>
        </fo:block>

    </xsl:template>

    <xsl:template match="y:import-spells">
        <xsl:apply-templates select="document(@href)/y:grimoire/yb:description"/>

        <fo:block
            font-weight="bold"
            color="{$colour}"
            font-size="{$font-large}"
            font-family="{$font-heading}"
            line-height="{$font-large}"
            space-after="{$font-medium}"
            text-align="start"
            keep-with-next="always">

            List of <xsl:value-of select="document(@href)/y:grimoire/@name"/> Spells
        </fo:block>
        
        <fo:block space-after.optimum="12pt">
            <xsl:apply-templates select="document(@href)/y:grimoire/y:spell[not(y:prerequisite)]" mode="list">
                <xsl:sort select="@level" data-type="number"/>
                <xsl:sort select="@name" data-type="text"/>
            </xsl:apply-templates>
        </fo:block>

        <fo:block>
            <xsl:text> </xsl:text>
        </fo:block>
        
        <fo:block
            font-weight="bold"
            color="{$colour}"
            font-size="{$font-large}"
            font-family="{$font-heading}"
            line-height="{$font-large}"
            space-after="{$font-medium}"
            space-before="{$font-medium}"
            text-align="start"
            keep-with-next="always">

            Spell Descriptions
        </fo:block>
    
        <xsl:choose>
            <xsl:when test="@type">
                <xsl:variable name="type" select="@type"/>
                <xsl:apply-templates select="document(@href)/y:grimoire/y:spell[@type=$type]">
                    <xsl:sort select="@name" data-type="text"/>
                    <xsl:sort select="@level" data-type="number"/>
                </xsl:apply-templates>
            </xsl:when>

            <xsl:otherwise>
                <xsl:apply-templates select="document(@href)/y:grimoire/y:spell">
                    <xsl:sort select="@name" data-type="text"/>
                    <xsl:sort select="@level" data-type="number"/>
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="y:spell">
        <fo:block
                font-weight="bold"
                font-style="italic"
                color="{$colour}"
                font-size="10pt"
                font-family="sans-serif"
                line-height="12pt"
                space-after.optimum="0pt"
                text-align="start">

            <xsl:value-of select="@name"/>,
            <xsl:if test="@level='0'">
                Multi-level
            </xsl:if>
            <xsl:if test="@level &gt; 0">
                Level <xsl:value-of select="@level"/>
            </xsl:if>
        </fo:block>

        <fo:table background-color="{$lightcolour}" table-layout="fixed">
            <fo:table-column column-width="4cm"/>
            <fo:table-column column-width="4cm"/>

            <fo:table-body font-size="10pt" font-family="Times">
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block font-family="Times">
                            <fo:inline font-weight="bold">Time: </fo:inline>
                            <xsl:if test="y:time">
                                <xsl:value-of select="y:time"/>
                            </xsl:if>
                            <xsl:if test="not(y:time)">
                                Action
                            </xsl:if>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block font-family="Times">
                            <fo:inline font-weight="bold">Range: </fo:inline>
                            <xsl:choose>
                                <xsl:when test="y:range='Reach'">Reach (3m)</xsl:when>
                                <xsl:when test="y:range='Short'">Short (25m)</xsl:when>
                                <xsl:when test="y:range='Medium'">Medium (100m)</xsl:when>
                                <xsl:when test="y:range='Long'">Long (250m)</xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="y:range"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>

                <fo:table-row>
                    <fo:table-cell>
                        <fo:block font-family="Times">
                            <fo:inline font-weight="bold">Duration: </fo:inline>
                            <xsl:value-of select="y:duration"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block font-family="Times">
                            <fo:inline font-weight="bold">Resist: </fo:inline>
                            <xsl:value-of select="y:resist"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>

                <xsl:if test="y:area">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block font-family="Times">
                                <fo:inline font-weight="bold">Area: </fo:inline>
                                <xsl:value-of select="y:area"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </xsl:if>

                <xsl:if test="y:radius">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block font-family="Times">
                                <fo:inline font-weight="bold">Radius: </fo:inline>
                                <xsl:choose>
                                    <xsl:when test="y:radius='Tiny'">Tiny (1m)</xsl:when>
                                    <xsl:when test="y:radius='Small'">Small (3m)</xsl:when>
                                    <xsl:when test="y:radius='Medium'">Medium (6m)</xsl:when>
                                    <xsl:when test="y:radius='Large'">Large (10m)</xsl:when>
                                    <xsl:when test="y:radius='Huge'">Huge (15m)</xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="y:radius"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </xsl:if>

                <xsl:if test="y:prerequisite">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block font-family="Times">
                                <fo:inline font-weight="bold">Prerequisites: </fo:inline>
                                <xsl:value-of select="y:prerequisite"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </xsl:if>

            </fo:table-body>
        </fo:table>

        <fo:block
            font-size="10pt"
            font-family="Times"
            line-height="12pt"
            space-after.optimum="0pt">

            <xsl:apply-templates select="yb:description"/>
        </fo:block>


    </xsl:template>

</xsl:stylesheet>

