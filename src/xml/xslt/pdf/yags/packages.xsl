<?xml version="1.0"?>
<!--
    Stylesheet transform for Yagsbook to PDF.

    Handles all advantage/disadvantage styles.

    Author:  Samuel Penn
    Version: $Revision: 1.11 $
    Date:    $Date: 2007/08/29 19:28:21 $

    Copyright 2012 Samuel Penn, http://yagsbook.sourceforge.net.
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<xsl:stylesheet xmlns:yb="http://yagsbook.sourceforge.net/xml"
                xmlns:y="http://yagsbook.sourceforge.net/xml/yags"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version="1.0">



    <xsl:template match="y:packages">
        <fo:block
            color="{$colour}"
            font-size="{$font-large}"
            font-family="{$font-body}"
            font-weight="bold" 
            space-after.optimum="{$font-medium}"
            line-height="{$font-large}">

            <xsl:value-of select="@title"/>
        </fo:block>
        <xsl:apply-templates select="yb:description"/>

        <xsl:apply-templates select="y:package">
            <xsl:sort data-type="text" select="@name"/>
        </xsl:apply-templates>
    </xsl:template>


    <!--
        Advantage

        Standard advantage template. The advantage has a name and a cost,
        followed by one or more paragraph blocks of description.
     -->
    <xsl:template match="y:package">
        <fo:block
            font-size="{$font-medium}"
            font-family="{$font-body}"
            line-height="{$font-large}"
            space-before="{$font-large}"
            space-after.optimum="{$font-large}"
            font-style="normal"
            keep-together.within-column="always">
            
            <fo:block font-weight="bold" color="{$colour}">
                <xsl:value-of select="@name"/>
            </fo:block>
            
            <xsl:apply-templates select="yb:description"/>

            <xsl:if test="y:attributes">
                <xsl:apply-templates select="y:attributes"/>
            </xsl:if>

            <xsl:if test="y:skills">
                <xsl:apply-templates select="y:skills"/>
            </xsl:if>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="y:package/y:attributes">
        <fo:block
            font-size="{$font-medium}"
            font-family="{$font-body}"
            line-height="{$font-large}"
            start-indent="1em"
            space-after.optimum="0pt">
            
            <fo:block font-weight="bold">Attributes</fo:block>

            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="y:package/y:attributes/y:attribute">
        <fo:block
            font-size="{$font-medium}"
            font-family="{$font-body}"
            line-height="{$font-large}"
            space-after.optimum="0pt">

            <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@score"/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="y:package/y:attributes/y:choice">
        <fo:block
            font-size="{$font-medium}"
            font-family="{$font-body}"
            line-height="{$font-large}"
            space-after.optimum="0pt">
            
            <xsl:text>One of: </xsl:text>
            
            <xsl:for-each select="y:attribute">
                <xsl:if test="position() > 1">
                    <fo:inline font-style="italic"> or </fo:inline>
                </xsl:if>
                <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@score"/>    
            </xsl:for-each>

            <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@score"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="y:package/y:skills">
        <fo:block
            font-size="{$font-medium}"
            font-family="{$font-body}"
            line-height="{$font-large}"
            start-indent="1em"
            space-after.optimum="0pt">
            
            <fo:block font-weight="bold">Skills</fo:block>

            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="y:package/y:skills/y:skill">
        <fo:block
            font-size="{$font-medium}"
            font-family="{$font-body}"
            line-height="{$font-large}"
            space-after.optimum="0pt">

            <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@score"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="y:package/y:skills/y:choice">
        <fo:block
            font-size="{$font-medium}"
            font-family="{$font-body}"
            line-height="{$font-large}"
            space-after.optimum="0pt">
            
            <xsl:text>One of: </xsl:text>
            
            <xsl:for-each select="y:skill">
                <xsl:if test="position() > 1">
                    <fo:inline font-style="italic"> or </fo:inline>
                </xsl:if>
                <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@score"/>    
            </xsl:for-each>

            <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@score"/>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
